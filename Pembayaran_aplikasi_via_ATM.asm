.model small
.code
org 100h
start:
	jmp mulai
	nama db 123,10,'PIN :$'
	
	masukan db 13,10,'<<Silakan masukan kode :>> $'
	kode db 13,10,'Masukan 5 angka corporate code : $'
	tampung_nama db 30,?,30 dup(?)
daftar1 db 13,10,'01 Ganti PIN'
	db 13,10,'02 Transfer'
	db 13,10,'03 Pembayaran','$'
daftar2 db 13,10,'0301 Kartu Kredit KIA'
	db 13,10,'0302 Pendidikan'
	db 13,10,'0303 Cicilan' 
	db 13,10,'0304 Ticketing'
	db 13,10,'0305 PLN'
	db 13,10,'0306 Seluler','$'
daftar3 db 13,10,'0401 100.000'
	db 13,10,'0402 200.000'
	db 13,10,'0402 500.000'
	db 13,10,'0402 1.000.000','$'

mulai:
	mov ah,09h
	lea dx,nama
	int 21h
	mov ah,0ah
	lea dx,tampung_nama
	int 21h
	push dx

	mov ah,09h
	mov dx,offset daftar1
	int 21h
	mov ah,09h
	mov dx,offset masukan
	int 21h
	mov ah,01h
	int 21h
	mov bh,al
	mov ah,01h
	int 21h
	mov bl,al
	int 21h

	cmp bh,'0'
	cmp bl,'3'
	jmp page2
	jne error_msg
page2:
	mov ah,09h
	mov dx,offset daftar2
	int 21h
	mov ah,09h
	mov dx,offset masukan
	int 21h
	mov ah,01h
	int 21h
	mov ah,01h
	int 21h
	mov bl,al
	int 21h
	cmp bh,'0'
	cmp bl,'9'

	jmp page3
	jmp error_msg
page3:
	mov ah,09h
	mov dx,offset daftar3
	int 21h
	mov ah,09h
	mov dx,offset masukan
	int 21h
    mov ah,01h
	int 21h
	mov bh,al
	mov ah,01h
	int 21h
	mov bl,al
	int 21h

	cmp bh,'0'
	cmp bl,'4'  
	
	jmp proses
	jne error_msg

error_msg:
	mov ah,09h
	mov dx,offset error_msg
	int 21h
	int 20h
proses:
	mov ah,09h
	mov dx,offset kode
	int 21h

masuk : mov ah,01h
	int 21h
	cmp al,'5'
	je lewat1

lewat1 : mov ah,01h
	int 21h
	cmp al,'4'
	je lewat2

lewat2 : mov ah,01h
	int 21h
	cmp al,'3'
	je lewat3

lewat3 : mov ah,01h
	int 21h
	cmp al,'2'
	je lewat4

lewat4 : mov ah,01h
	int 21h
	cmp al,'1'
	je lewat5

	jne masuk
lewat5 : mov ah,09h
	mov dx,offset hasil1
	int 21h
	int 20h

hasil1:
   mov ah,09h
   lea dx,teks1
   int 21h
   int 20h

teks1 db 13,10,'Aplikasi Pembayaran'
	db 13,10,'No	:001'
	db 13,10,'Instansi :UAS_BR'
	db 13,10,'Nama :Dhea_Aprila'
	db 13,10,'Keterangan :0306'
	db 13,10,'Jumlah :0404 $'
end start